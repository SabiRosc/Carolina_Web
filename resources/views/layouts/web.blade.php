<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Carolina Van Pampus | Coach de Vida para Madres</title>

    <link rel="shortcut icon" href="{{ asset('/image/C.jpg') }}"/>
    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
    <!-- Styles -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/flexslider.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/css/animate.css') }}" type="text/css">


</head>
<body id="app-layout" style="padding-top: 2%">
<nav class="navbar navbar-default" role="navigation" style="background-color: white; border-color: white;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <a class="navbar-brand"><img src="{{ asset('/images/logo.png') }}" alt="logo" style="box-shadow: none"></a>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-3">

            <ul class="nav navbar-nav navbar-left">
                <li><a href="{{ url('/') }}">Inicio</a></li>
                <li><a href="{{ url('/acerca') }}">Acerca de</a></li>
                <li><a href="{{ url('/coaching') }}">Coaching</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/madres') }}">Madres Aliadas</a></li>
                <li><a href="{{ url('/contacto') }}">Contacto</a></li>
                <li><a href="{{ url('/blog') }}">Blog</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<div class="container">
    @if (Session::has('message'))
        <div class="flash alert-info">
            <p class="panel-body">
                {{ Session::get('message') }}
            </p>
        </div>
    @endif
    @if ($errors->any())
        <div class='flash alert-danger'>
            <ul class="panel-body">
                @foreach ( $errors->all() as $error )
                    <li>
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
@yield('content')

<script id="mcjs">!function (c, h, i, m, p) {
        m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
    }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/0674eb703224d87b875004b22/f1ea21eda82c2ab11d584576b.js");</script>
<script src="{{ asset('/js/jquery.min-2.1.3.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min-3.3.1.js') }}"></script>
<script src="{{ asset('/js/bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/jquery.flexslider.js') }}"></script>
</body>
</html>
