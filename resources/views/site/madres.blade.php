@extends('layouts.web')

@section('content')
    <div class="container" id="coach">
        <div class="row">
            <div class="col-md-12 animate-box fadeInUp animated">
                <img src="{{ url('/images/6-1.jpg') }}" alt="" class="img-responsive">
                <br>
                <br>
                <h3 style="font-size: medium">Esta sección está dedicada a todas esas mamás que día a día asumen el reto de ser Madres & Coaches
                    al mismo tiempo y en sus propios términos. </h3>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-8 col-md-offset-3 col-sm-offset-2 text-center">
                <a href="" class="big-button">Únete a Las Madres Aliadas</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 ">
                <div class="card">
                    <div class="image1">
                        <img src="{{ url('/images/22.1.jpg') }}" width="100%"
                             style="box-shadow: none; height: 330px; width: 330px; object-fit: cover">
                    </div>
                    <div class="text">
                        <h3>Paola Gonzalez</h3>
                        <h4 style=" color: #FF7057;">Kardioyoga | Instructora de Yoga</h4>
                        <h4 style="font-style: italic; font-size: 80%">Madre de: <br>Sophie de 22 meses</h4>
                        <p>Kardioyoga es una combinación de las palabras cardio y yoga. Fusiono
                            dos estilos de yoga vinyasa y ashantanga (posturas de yoga) con
                            ejercicios de cardio. En cada una de mis clases incluyo ejercicios de
                            respiración, meditación y aromateriapia. Quiero mostrar una versión
                            diferente del yoga, enseñar a respirar correctamente, a conocer tu cuerpo
                            y mente y sobre todo a que sonrias por dentro y por fuera.</p>
                        <br>
                        <p style="font-style: italic; color:black; text-align: center;">La maternidad ha sido para mi
                            una retrospección. Me ha llevado al lado más sublime y humano.</p>
                        <br>
                        <ul class="col-md-offset-1 col-sm-offset-1 col-xs-offset-3">
                            <li><a href="mailto:kardioyoga@gmail.com" class="fa-lg fa fa-envelope-o"></a></li>
                            <li><a href="https://www.facebook.com/kardioyoga/" class="fa-lg fa fa-facebook-square"></a>
                            </li>
                            <li><a href="https://www.instagram.com/kardioyoga/" class="fa-lg fa fa-instagram"></a></li>
                            <li><a href="https://www.pinterest.co.uk/pagagova/" class="fa-lg fa fa-pinterest"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <div class="image2">
                        <img src="{{ url('/images/21.jpg') }}" width="100%"
                             style="box-shadow: none; height: 330px; width: 330px; object-fit: cover">
                    </div>
                    <div class="text">
                        <h3>Patricia Galarza</h3>
                        <h4 style=" color: #FF7057;">Realmente Natural</h4>
                        <h4 style="font-style: italic; font-size: 80%">Madre de: <br>Isabella de 16 años y Antonella de
                            1 año</h4>
                        <p> Una guía práctica informal basada en mi experiencia de alimentación balanceada y ejercicio
                            pero sin extremos, ya que, me da mucha nota estos aprendizajes que he tenido y quiero
                            compartirlos a quien pueda servir.</p>
                        <br>
                        <p style="font-style: italic; color:black; text-align: center;"> Ser mamá para mí significa
                            traer al mundo a tu(s) eterno(s) maestro(s) de vida, entregándole(s) tu corazón eterna e
                            incondicionalmente.</p>
                        <br>
                        <ul class="col-md-offset-5 col-sm-offset-5 col-xs-offset-5">
                            <li><a href="mailto:Realmentenatural96@gmail.com" class="fa-lg fa fa-envelope-o"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="card">
                    <div class="image">
                        <img src="{{ url('/images/20.jpg') }}" width="100%"
                             style="box-shadow: none; height: 330px; width: 330px; object-fit: cover">
                    </div>
                    <div class="text">
                        <h3>Dayana del Valle</h3>
                        <h4 style=" color: #FF7057;">Life Coach | Programa Autores de Éxito</h4>
                        <h4 style="font-style: italic; font-size: 80%">Madre de: <br>Daniele de 17 y Anna Paola de 9
                        </h4>
                        <p>-Programa Autores de Éxito para escribir, para personas cuyo sueño ha sido escribir un libro,
                            adaptado y personalizado a las necesidades del cliente. Sesiones personalizadas, modulos de
                            trabajo y material complementario. 8 semanas para escribir con un experto en la materia.
                            <br>
                            -Sesiones de Coaching.
                        </p>
                        <br>
                        <p style="font-style: italic; color:black; text-align: center;">Para mí la maternidad es la más
                            hermosa tarea que una como ser humano puede experimentar,creo que no existen palabras para
                            describirla.
                        </p>
                        <br>
                        <ul class="col-md-offset-1 col-sm-offset-1 col-xs-offset-3">
                            <li><a href="http://dayanadelvalle.com/" class="fa-lg fa fa-globe"></a></li>
                            <li><a href="mailto:coachdayanadelvalle@gmail.com" class="fa-lg fa fa-envelope-o"></a></li>
                            <li><a href="https://www.facebook.com/dayanadelvallecoach/"
                                   class="fa-lg fa fa-facebook-square"></a></li>
                            <li><a href="https://www.instagram.com/coach.dayanadelvalle/"
                                   class="fa-lg fa fa-instagram"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="publicidad">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-lg-2 col-xs-6 col-xs-offset-1 col-md-offset-2">
                    <img src="{{ url('/images/libro.png') }}" class="libro"
                         style="box-shadow: none; position: static; height: 200%; width: 200%;">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-1 col-sm-offset-2 col-xs-offset-1 col-md-push-1 text-center animate-box fadeInUp animated"
                     style="padding-top: 0%">
                    <h3 style="color: white">Conviviendo Con Nuestro Pequeño Coachee</h3>
                    <h4 style="color: black">Asumiendo el desafío de ser Padres & Coaches al mismo tiempo</h4>
                    <br>
                    <button class=" btn btn-default" style="background-color:white">
                        <a style="color: #FF7057" href="https://www.amazon.com/dp/1548089729/ref=cm_sw_r_cp_api_mbbTzb4Y2P8SR">Cómpralo
                            Ya</a></button>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <footer>
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12  col-sm-12 col-md-offset-1">
                            <div class="col-md-6 col-sm-6">
                                <p>Copyright © 2017 <a>Carolina Van Pampus</a>. All Rights Reserved. </p>
                                <p>Diseñado y desarrollado:<a>Sabina Rosciano</a></p>
                                <p>Fotografia:<a>Lorena Sanz</a></p>
                            </div>
                            <div class="col-md-5 col-sm-4 col-xs-12">
                                <a href="{{ url('/') }}" style="margin-right: 1%">Inicio</a>
                                <a href="{{ url('/acerca') }}" style="margin-right: 1%">Acerca de</a>
                                <a href="{{ url('/coaching') }}" style="margin-right: 1%">Coaching</a>
                                <a href="{{ url('/madres') }}" style="margin-right: 1%">Madres Aliadas</a>
                                <a href="{{ url('/contacto') }}" style="margin-right: 1%">Contacto</a>
                                <a href="{{ url('/blog') }}">Blog</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
@endsection
