@extends('layouts.web')

@section('content')
    <div class="container">
        <div class="col-md-12 animate-box fadeInUp animated">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <img src="{{ url('/images/1.jpg') }}" alt="left" class="img-responsive">
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="{{ url('/images/15.jpg') }}" alt="top" class="img-responsive">
                </div>
                <div class="col-md-4 col-sm-4">
                    <img src="{{ url('/images/11.jpg') }}" alt="bottom" class="img-responsive"
                         style="padding-top: 7%">
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 animate-box fadeInUp animated">
                <h2 style="color: #FF7057;">Carolina Van Pampus</h2>
                <p>Venezolana, licenciada en Estudios Liberales de la Universidad Metropolitana y con un par de
                    Maestrías en el área de Finanzas y Comercio Internacional, área en la que me he desarrollado
                    profesionalmente en los últimos 10 años. Sin embargo, mi título más empoderante e importante ha sido
                    el de “MAMI”.
                    <br>
                    A mi maternidad le debo, además de la maravillosa compañía de mi hija, ALANA:
                    <br>
                    1) El descubrimiento de mi vocación como COACH INTEGRAL DE VIDA certificada por la ACCA (Academia de
                    Coaching y Capacitación Americana).
                    <br>
                    2) La revelación de cuán importante resulta “ESTAR PRESENTES” como madres en el continuo desarrollo
                    de nuestros hij@s, apagando los switches emocionales del “qué dirán” o de todo lo que “según
                    nosotras” FALTA por hacer en casa.
                    <br>
                    3) El RE-CONOCERME a través del amor que siento por mi hija, el cual me ha permitido VALORARME y
                    MOTIVARME.
                    <br>
                    4) La necesidad de HALLAR LAS RESPUESTAS a las muchas preguntas que surgieron junto a mi nuevo roll
                    de mamá y, a su vez, a encauzar esas necesidades de SER UNA MEJOR VERSIÓN DE MÍ MISMA y de
                    EMPODERARME que nacieron junto a mi hija.
                    <br>
                    Aquí verás que las madres no siempre necesitamos de alguien con “mucha más experiencia”, al
                    contrario, nosotras mismas con nuestros instintos y las herramientas adecuadas somos más que
                    suficientes. ¡Bienvenidos a mi blog!

                </p>
            </div>
        </div>
    </div>
    <div class="publicidad">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-lg-2 col-xs-6 col-xs-offset-1 col-md-offset-2">
                    <img src="{{ url('/images/libro.png') }}" class="libro" style="box-shadow: none; position: static; height: 200%; width: 200%;">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-1 col-sm-offset-2 col-xs-offset-1 col-md-push-1 text-center animate-box fadeInUp animated"
                     style="padding-top: 0%">
                    <h3 style="color: white">Conviviendo Con Nuestro Pequeño Coachee</h3>
                    <h4 style="color: black">Asumiendo el desafío de ser Padres & Coaches al mismo tiempo</h4>
                    <br>
                    <button class=" btn btn-default" style="background-color:white"><a style="color: #FF7057" href="https://www.amazon.com/dp/1548089729/ref=cm_sw_r_cp_api_mbbTzb4Y2P8SR">Cómpralo Ya</a></button>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12  col-sm-12 col-md-offset-1">
                        <div class="col-md-6 col-sm-6">
                            <p>Copyright © 2017 <a>Carolina Van Pampus</a>. All Rights Reserved. </p>
                            <p>Diseñado y desarrollado:<a>Sabina Rosciano</a></p>
                            <p>Fotografia:<a>Lorena Sanz</a></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                            <a href="{{ url('/') }}" style="margin-right: 1%">Inicio</a>
                            <a href="{{ url('/acerca') }}" style="margin-right: 1%">Acerca de</a>
                            <a href="{{ url('/coaching') }}" style="margin-right: 1%">Coaching</a>
                            <a href="{{ url('/madres') }}" style="margin-right: 1%">Madres Aliadas</a>
                            <a href="{{ url('/contacto') }}" style="margin-right: 1%">Contacto</a>
                            <a href="{{ url('/blog') }}">Blog</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection