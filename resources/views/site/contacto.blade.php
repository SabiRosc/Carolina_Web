@extends('layouts.web')

@section('content')

    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="col-md-6 col-md-offset-2">
                <div class="animate-box fadeInUp animated">
                    <h2 style="color: black">Sigueme en:</h2>
                    <br>
                    <a href="https://twitter.com/CarolVanPampus" class="fa-2x fa fa-twitter"
                       style="padding-right: 2%"></a>
                    <a href="https://www.facebook.com/carolvanpampus/" class="fa-2x fa fa-facebook-official"
                       style="padding-right: 2%"></a>
                    <a href="https://www.instagram.com/carolvanpampus/" class="fa-2x fa fa-instagram"
                       style="padding-right: 2%"></a>
                    <a href="" class="fa-2x fa fa-youtube-play" style="padding-right: 2%"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-md-offset-1">
            <div class="col-md-6 col-md-offset-2">
                <div class="animate-box fadeInUp animated">
                    <h2 style="color: black">Escribeme un mensaje:</h2>
                    <br>
                    <form action="mailto:CarolinaVanPampus@gmail.com" method="post" enctype="text/plain">
                        Nombre:<br>
                        <input type="text" class="form-control"><br>
                        Apellido:<br>
                        <input type="text" class="form-control"><br>
                        E-mail:<br>
                        <input type="text" class="form-control"><br>
                        Asunto:<br>
                        <textarea class="form-control" rows="3"></textarea>
                        <br>
                        <input class="btn btn-default" type="submit" value="Enviar">
                        <input class="btn btn-default" type="reset" value="Limpiar">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12  col-sm-12 col-md-offset-1">
                        <div class="col-md-6 col-sm-6">
                            <p>Copyright © 2017 <a>Carolina Van Pampus</a>. All Rights Reserved. </p>
                            <p>Diseñado y desarrollado:<a>Sabina Rosciano</a></p>
                            <p>Fotografia:<a>Lorena Sanz</a></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                            <a href="{{ url('/') }}" style="margin-right: 1%">Inicio</a>
                            <a href="{{ url('/acerca') }}" style="margin-right: 1%">Acerca de</a>
                            <a href="{{ url('/coaching') }}" style="margin-right: 1%">Coaching</a>
                            <a href="{{ url('/madres') }}" style="margin-right: 1%">Madres Aliadas</a>
                            <a href="{{ url('/contacto') }}" style="margin-right: 1%">Contacto</a>
                            <a href="{{ url('/blog') }}">Blog</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
