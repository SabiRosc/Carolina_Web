@extends('layouts.web')

@section('content')
    <div class="head-top">
        <div class="row">
            <div class="title text-center">
                <h1>Carolina Van Pampus</h1>
                <h3>Coach de Vida para Madres</h3>
            </div>
        </div>
    </div>
    <div class="publicidad">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-lg-2 col-xs-6 col-xs-offset-1 col-md-offset-2">
                    <img src="{{ url('/images/libro.png') }}" class="libro"
                         style="box-shadow: none; position: static; height: 200%; width: 200%;">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-1 col-sm-offset-2 col-xs-offset-1 col-md-push-1 text-center animate-box fadeInUp animated"
                     style="padding-top: 0%">
                    <h3 style="color: white">Conviviendo Con Nuestro Pequeño Coachee</h3>
                    <h4 style="color: black">Asumiendo el desafío de ser Padres & Coaches al mismo tiempo</h4>
                    <br>
                    <button class=" btn btn-default" style="background-color:white"><a style="color: #FF7057"href="https://www.amazon.com/dp/1548089729/ref=cm_sw_r_cp_api_mbbTzb4Y2P8SR">Cómpralo
                            Ya</a></button>
                </div>
            </div>
        </div>
    </div>
    <div class="mid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-1">
                    <div class="col-md-6 animate-box fadeInUp animated">
                        <div class="animate-box fadeInUp animated">
                            <img src="{{ asset('/images/3-1.jpg') }}">
                        </div>
                    </div>
                    <div class="col-md-4 animate-box fadeInUp animated">
                        <h2 style="font-size: 180%">¡Bienvenidas a mi Blog!</h2>
                        <p>Estoy muy agradecida de que hayas llegado hasta aquí y espero que como Mamá & Coach esta
                            página te ayude a empoderarte a través de tu maternidad y algunas herramientas prácticas de
                            Coaching.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offset-1">
                    <div class="col-md-6 col-md-push-6 animate-box fadeInUp animated">
                        <div class="animate-box fadeInUp animated">
                            <img src="{{ asset('/images/5-1.jpg') }}">
                        </div>
                    </div>
                    <div class="col-md-5 col-md-pull-6 animate-box fadeInUp animated">
                        <h2 style="font-size: 180%">Mi meta</h2>
                        <p>Ayudar a otras madres que, al igual que yo, quieren asumir el reto de compaginar sus roles
                            como Madres & Coaches, porque aunque no lo sepan aún, ese es el desafío que se nos plantea
                            hoy en día: Trazar límites desde el respeto y, al mismo tiempo, acompañar y motivar a
                            nuestros hijos(a) a buscar las respuestas dentro de sí mismos.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offset-1">
                    <div class="col-md-6 animate-box fadeInUp animated">
                        <div class="animate-box fadeInUp animated">
                            <img src="{{ asset('/images/14-1.jpg') }}">
                        </div>
                    </div>
                    <div class="col-md-4 animate-box fadeInUp animated">
                        <h2 style="font-size: 180%">¿Tienes miedos, dudas?</h2>
                        <p> ¿Te sientes insegura sobre si serás una “buena madre”? ¿Quieres ser capaz de materializar
                            esa mejor versión de ti misma que ha surgido de la mano con tu maternidad? ¿Todas las
                            anteriores?.
                            <br>
                            <br>
                            La buena noticia es que las respuestas están más cerca de lo que piensas y en este blog
                            encontrarás experiencias, herramientas y, sobretodo, mucho apoyo, para que puedas ser una
                            Mamá & Coach en tus propios términos.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12  col-sm-12 col-md-offset-1">
                        <div class="col-md-6 col-sm-6">
                            <p>Copyright © 2017 <a>Carolina Van Pampus</a>. All Rights Reserved. </p>
                            <p>Diseñado y desarrollado:<a>Sabina Rosciano</a></p>
                            <p>Fotografia:<a>Lorena Sanz</a></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                            <a href="{{ url('/') }}" style="margin-right: 1%">Inicio</a>
                            <a href="{{ url('/acerca') }}" style="margin-right: 1%">Acerca de</a>
                            <a href="{{ url('/coaching') }}" style="margin-right: 1%">Coaching</a>
                            <a href="{{ url('/madres') }}" style="margin-right: 1%">Madres Aliadas</a>
                            <a href="{{ url('/contacto') }}" style="margin-right: 1%">Contacto</a>
                            <a href="{{ url('/blog') }}">Blog</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
