@extends('layouts.web')

@section('content')
    <div class="container" id="coach">
        <div class="row">
            <div class="col-md-12 animate-box fadeInUp animated">
                <img src="{{ url('/images/10-1.jpg') }}" alt=""
                     class="img-responsive">
                <p>
                <h2 style="font-size: 150%">El Coaching</h2>
                <br>
                Es una metodología de acompañamiento entre el Coach y el Coachee (o persona que recibe el
                entrenamiento) con la finalidad de ir de un punto A (realidad actual) a un punto B (realidad
                deseada).
                <br>
                Mi objetivo entonces es ayudar a otras mamás a compaginar sus roles de Mamá & Coach, porque como
                Madres creo que somos el acompañante designado por excelencia de nuestros hijos (o pequeños
                coachees).
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 animate-box fadeInUp animated">
                <img src="{{ url('/images/9-1.jpg') }}" alt=""
                     class="img-responsive">
                <p>
                <h2 style="font-size: 150%">- Sesiones de Coaching Personalizadas</h2>
                <br>
                Mi propuesta es que tengamos una primera sesión libre de costos para que nos evaluemos como Coach y
                Coachee. Luego de esa primera sesión, llegaremos a un acuerdo para afinar los detalles sobre el
                inicio del proceso de Coaching (de ser el caso).
                <br>
                <br>
                <h2 style="font-size: 150%">- Conversatorios</h2>
                <br>
                La propuesta es reunirnos para conversar sobre temas de interés, intercambiar experiencias,
                herramientas y risas para salir empoderadas y motivadas a ser esa mejor versión de nosotras mismas.
                </p>
            </div>
        </div>
    </div>
    <div class="publicidad">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3 col-lg-2 col-xs-6 col-xs-offset-1 col-md-offset-2">
                    <img src="{{ url('/images/libro.png') }}" class="libro"
                         style="box-shadow: none; position: static; height: 200%; width: 200%;">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-10 col-md-offset-1 col-sm-offset-2 col-xs-offset-1 col-md-push-1 text-center animate-box fadeInUp animated"
                     style="padding-top: 0%">
                    <h3 style="color: white">Conviviendo Con Nuestro Pequeño Coachee</h3>
                    <h4 style="color: black">Asumiendo el desafío de ser Padres & Coaches al mismo tiempo</h4>
                    <br>
                    <button class=" btn btn-default" style="background-color:white">
                        <a style="color: #FF7057"
                           href="https://www.amazon.com/dp/1548089729/ref=cm_sw_r_cp_api_mbbTzb4Y2P8SR">
                            Cómpralo Ya</a>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-md-offset-1">
                        <div class="col-md-6 col-sm-6">
                            <p>Copyright © 2017 <a>Carolina Van Pampus</a>. All Rights Reserved. </p>
                            <p>Diseñado y desarrollado:<a>Sabina Rosciano</a></p>
                            <p>Fotografia:<a>Lorena Sanz</a></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12">
                            <a href="{{ url('/') }}" style="margin-right: 1%">Inicio</a>
                            <a href="{{ url('/acerca') }}" style="margin-right: 1%">Acerca de</a>
                            <a href="{{ url('/coaching') }}" style="margin-right: 1%">Coaching</a>
                            <a href="{{ url('/madres') }}" style="margin-right: 1%">Madres Aliadas</a>
                            <a href="{{ url('/contacto') }}" style="margin-right: 1%">Contacto</a>
                            <a href="{{ url('/blog') }}">Blog</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
