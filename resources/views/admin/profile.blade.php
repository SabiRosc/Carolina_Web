@extends('layouts.weblog')

@section('title')

@endsection
@section('content')
    <br>
    <div class="col-md-10 col-sm-8 col-xs-10 col-md-offset-1 col-sm-offset-2 col-xs-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>{{ $user->name }}</h2>
                </div>
                <div class="panel-body">
                    <table>
                        Joined on {{$user->created_at->format('M d,Y \a\t h:i a') }}
                        <style>
                            .table-padding td {
                                padding: 3px 8px;
                            }
                        </style>
                        <tr>
                            <td>Total de Posts</td>
                            <td> {{$posts_count}}</td>
                            @if($author && $posts_count)
                                <td><a href="{{ url('/my-all-posts')}}">Mostrar Todos</a></td>
                            @endif
                        </tr>
                        <tr>
                            <td>Posts Publicados</td>
                            <td>{{$posts_active_count}}</td>
                            @if($posts_active_count)
                                <td><a href="{{ url('/user/'.$user->id.'/posts')}}">Mostrar Todos</a></td>
                            @endif
                        </tr>
                        <tr>
                            <td>Borradores</td>
                            <td>{{$posts_draft_count}}</td>
                            @if($author && $posts_draft_count)
                                <td><a href="{{ url('my-drafts')}}">Mostrar Todos</a></td>
                            @endif
                        </tr>
                    </table>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Últimos Posts</h3></div>
                <div class="panel-body">
                    @if(!empty($latest_posts[0]))
                        @foreach($latest_posts as $latest_post)
                            <p>
                                <strong><a href="{{ url('/'.$latest_post->slug) }}">{{ $latest_post->title }}</a></strong>
                                <span class="well-sm">On {{ $latest_post->created_at->format('M d,Y \a\t h:i a') }}</span>
                            </p>
                        @endforeach
                    @else
                        <p>No has escrito ningun post por ahora.</p>
                    @endif
                </div>
            </div>

        </div>
@endsection
