@extends('layouts.weblog')

@section('title')
    {{$title}}
@endsection

@section('content')
    <div class="col-md-12 col-md-offset-1">
        @if ( !$posts->count() )
            No hay posts por ahora. Has login y escribe un nuevo post!
        @else
            @foreach( $posts as $post )
                <div class="col-md-10">
                    <div class="card">
                        <div class="image">
                            <img src="{{ asset('images/posts/'. $post->name_image ) }}" alt="{{ $post->title }}"
                                 style="box-shadow: none; height: 400px; width: 800px; object-fit: cover">
                        </div>
                        <div class="text" style="border-top: none">
                            <h2><a href="{{ url('/'.$post->slug) }}">{{ $post->title }}</a>
                                @if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
                                    @if($post->active == '1')
                                        <button class="btn" style="float: right; color: white; background-color: #FF7057;">
                                            <a style="color: white; background-color: #FF7057; border: none;"
                                               href="{{ url('edit/'.$post->slug)}}">Edit Post</a></button>
                                    @else
                                        <button class="btn" style="float: right; color: white; background-color: #FF7057;">
                                            <a style="color: white; background-color: #FF7057; border: none;"
                                                    href="{{ url('edit/'.$post->slug)}}">Edit Draft</a></button>
                                        <br>
                                        <button class="btn bg-danger" style="float: right;">
                                            <a href="{{ url('activar/'.$post->id)}}">Publish Post</a>
                                        </button>
                                    @endif
                                @endif
                            </h2>
                            <h4>{{ $post->created_at->format('M d,Y \a\t h:i a') }} By
                                <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a>
                            </h4>
                            <br>
                            <article style="color: black;">
                                {!! str_limit($post->body, $limit = 1500, $end = '....... <a class="btn btn-default" href='.url("/".$post->slug).' style="color: white; background-color: #FF7057; border:none">Read More</a>') !!}
                            </article>
                        </div>
                    </div>
                </div>
            @endforeach
            {!! $posts->render() !!}
        @endif
    </div>
@endsection
