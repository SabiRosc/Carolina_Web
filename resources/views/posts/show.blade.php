@extends('layouts.weblog')

@section('post-image')
    @if($post)
        <img src="{{ asset('images/posts/'. $post->name_image ) }}" alt="{{ $post->title }}"
             style="box-shadow: none; width: 100%; max-height: 500px; max-width: 1200px; object-fit: cover">
    @endif
@endsection


@section('title')

    @if($post)
        <h1 style="font-size: 3em">
            <a href="">{{ $post->title }}</a>
        </h1>
        @if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
            <button class="btn" style="color: white; background-color: #FF7057; float: right">
                <a style="color: white; background-color: #FF7057; border: none;"
                   href="{{ url('edit/'.$post->slug)}}">Edit Post</a>
            </button>
        @endif
    @else
        Page does not exist
    @endif
@endsection

@section('title-meta')
    <p>{{ $post->created_at->format('M d,Y \a\t h:i a') }} By
        <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a>
    </p>
@endsection

@section('content')
    @if($post)
        <div>
            {!! $post->body !!}
        </div>
    @else
        404 error
    @endif
    <div>
        <h3>Deja un Comentario</h3>
    </div>
    <div class="fb-comments" data-href="{{ Request::url() }}}" data-numposts="5"
         data-width="1130"></div>
@endsection

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.10&appId=129464241031113";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
